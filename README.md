# README-GUIDELINES-FAQ



## Group purpose


This group is mainly for having access and sharing self-hosted Gitlab Runners among the SheepIt-Developers,
because Gitlab changed their opensource CI minutes paradigm.
Though you are naturally welcome to develop here, if cooperatively or not is your choice.


## Guidelines


Please prefix branches you create/maintain with your username (or a shorthand thereof) so it is clearly recognizable.

Don't mess with others developers branches, especially not without asking first.  
MR's are available, they don't bite, or coordinate somewhere else like Discord.

Don't upload huge files (100MB+) to the repos, don't repeat DaCool's past mistakes.  
(Suggestions to solve this issue are welcome. Git LFS?)

Don't be a dork. Respect others. Use common-sense.  
If in doubt, ask first.


## FAQ


### Why are all branches from the upstream repositories protected?

The repos in this group are intended to mirror upstream, any changes made to the mirrored branches would be overridden anyhow, 
so we save you the trouble and not let you push in the first places.
Please don't create MR's for these branches, they will just be silently closed.

### How do I get access?

Ask in our Discord in the #dev channel (or in #help-me if you don't have the developer role).  
Pinging @DaCool recommended & encouraged.   
([Discord Invite](https://discord.com/invite/GqMHJSQgVf))

Or file an issue in this repo.

### Gitlab Runner broken? Need Group/Repo setting changed?

Ask DaCool on Discord or file an issue.

### How are the Gitlab Runners setup?

The documentation, how-to's, scripts and more for the SheepIt Gitlab Runners is a work-in-progress.

### Why all these icons in the repos?

To differentiate them at a glance.  
In-case the ``sheepit-`` prefix didn't already tip you off.

### Why is this group called DaCoolX?

It was just a free namespace after my username occupied DaCool.  
I don't particularly care what this group is called, do you?

### Why am I still reading this?

I don't know, do you? :-)